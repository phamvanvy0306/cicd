echo 'deploy app to test server'
rm -rf $HOME/cicd
mkdir -p $HOME/cicd 
cd $HOME/cicd
#git clone https://glpat-w46sN4fSqWDf54ZrnLxX@gitlab.com/phamvanvy0306/cicd.git .
ssh {SSH_USER}@${SSH_SERVER_IP_TEST} "echo SSH_DEPLOY_PASSWORD | docker login -u SSH_DEPLOY_USER registry.gitlab.com --password-stdin"
git clone https://gitlab.com/phamvanvy0306/cicd.git .
#docker stack deploy --compose-file docker-compose.yml stackpython
docker-compose down
docker-compose up -d

